import Toybox.Lang;
import Toybox.WatchUi;

class TomatoClockDelegate extends WatchUi.BehaviorDelegate {

    var view as TomatoClockView;

    function initialize(view as TomatoClockView) {
        BehaviorDelegate.initialize();
        self.view = view;
    }

    function onMenu() as Boolean {
        WatchUi.pushView(new Rez.Menus.MainMenu(), new TomatoClockMenuDelegate(self.view), WatchUi.SLIDE_UP);
        return true;
    }

    function onSelect() as Boolean {
        if (self.view.isTimerRunning()) {
            self.view.togglePause();
        } else {
            self.view.startTimer();
        }
        return true;
    }

    function onBack() as Boolean {
        if (self.view.isTimerRunning()) {
            var dialog = new WatchUi.Confirmation("Exit?");
            WatchUi.pushView(
                dialog,
                new ConfirmationDelegate(),
                WatchUi.SLIDE_IMMEDIATE
            );
            return true;
        }
        return false;
    }
}