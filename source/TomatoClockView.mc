import Toybox.Graphics;
import Toybox.WatchUi;
import Toybox.Timer;
import Toybox.Lang;
import Toybox.Attention;

class TomatoClockView extends WatchUi.View {

    var timer as Timer.Timer? = null;
    var timeRemaining = 25 * 60; // 25 minutes in seconds
    var isPaused as Boolean = false;

    function initialize() {
        View.initialize();
        var timer = new Timer.Timer();
        timer.start(method(:currentTimerCallback), 1000, true);
    }

    function isTimerRunning() as Boolean {
        return timer != null;
    }

    function resetTimer() as Void {
        stopTimer();
        timeRemaining = 25 * 60;
    }

    // 在 onMenuItem 中添加重置选项
    function onMenuItem(item as Symbol) as Void {
        if (item == :item_reset) {
            resetTimer();
        }
        // 其他菜单项处理
    }
    // Load your resources here
    function onLayout(dc as Dc) as Void {
        setLayout(Rez.Layouts.MainLayout(dc));
    }

    function setTime(minutes as Number) as Void {
        timeRemaining = minutes * 60;
        stopTimer();
        WatchUi.requestUpdate();
    }

    function startTimer() as Void {
        if (timer == null) {
            timer = new Timer.Timer();
            timer.start(method(:timerCallback), 1000, true);
            isPaused = false;
        }
    }

    function stopTimer() as Void {
        if (timer != null) {
            timer.stop();
            timer = null;
            isPaused = true;
        }
    }

    function onShow() as Void {
    }

    function currentTimerCallback() as Void {
        WatchUi.requestUpdate();
    }

    function timerCallback() as Void {
        timeRemaining -= 1;
        if (timeRemaining <= 0) {
            stopTimer();
            // 震动和提示音
            if (Attention has :playTone) {
                Attention.playTone(Attention.TONE_ALARM);
            }
            if (Attention has :vibrate) {
                var vibeData = [new Attention.VibeProfile(50, 2000)];
                Attention.vibrate(vibeData);
            }
        }
        WatchUi.requestUpdate();
    }

    // Update the view
    function onUpdate(dc as Dc) as Void {
        View.onUpdate(dc);
        var clockTime = System.getClockTime();
        var timeString = Lang.format("$1$:$2$:$3$",
            [clockTime.hour.format("%02d"), clockTime.min.format("%02d"), clockTime.sec.format("%02d")]);
        
        // 显示当前时间
        dc.setColor(Graphics.COLOR_WHITE, Graphics.COLOR_BLACK);
        dc.drawText(dc.getWidth()/2, dc.getWidth()/4, Graphics.FONT_MEDIUM, timeString, Graphics.TEXT_JUSTIFY_CENTER);
        
        // 显示倒计时
        dc.setColor(Graphics.COLOR_RED, Graphics.COLOR_BLACK);
        dc.drawText(dc.getWidth()/2, dc.getHeight()/2, Graphics.FONT_MEDIUM, 
            "Time Remaining: \n" + formatTime(timeRemaining) + "\n" + 
            (isTimerRunning() ? (isPaused ? "Paused" : "Running") : "Stopped"), 
            Graphics.TEXT_JUSTIFY_CENTER);
    }

    // Called when this View is removed from the screen. Save the
    // state of this View here. This includes freeing resources from
    // memory.
    function onHide() as Void {
    }

    function formatTime(seconds) as String {
        var minutes = Math.floor(seconds / 60);
        var secs = seconds % 60;
        return (minutes < 10 ? "0" + minutes : minutes) + ":" + (secs < 10 ? "0" + secs : secs);
    }

    function togglePause() as Void {
        if (isTimerRunning()) {
            if (isPaused) {
                // 恢复计时
                timer.start(method(:timerCallback), 1000, true);
                isPaused = false;
            } else {
                // 暂停计时
                timer.stop();
                isPaused = true;
            }
        }
        WatchUi.requestUpdate();
    }

}
