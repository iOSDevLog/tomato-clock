import Toybox.Lang;
import Toybox.System;
import Toybox.WatchUi;

class TomatoClockMenuDelegate extends WatchUi.MenuInputDelegate {

    var view as TomatoClockView;

    function initialize(view as TomatoClockView) {
        MenuInputDelegate.initialize();
        self.view = view;
    }

    function onMenuItem(item as Symbol) as Void {
        view.stopTimer();
        if (item == :item_reset) {
            System.println("item Reset");
            self.view.resetTimer();
        } else if (item == :item_5_min) {
            view.setTime(5);
        } else if (item == :item_10_min) {
            view.setTime(10);
        } else if (item == :item_15_min) {
            view.setTime(15);
        } else if (item == :item_20_min) {
            view.setTime(20);
        } else if (item == :item_25_min) {
            view.setTime(25);
        }
    }

}